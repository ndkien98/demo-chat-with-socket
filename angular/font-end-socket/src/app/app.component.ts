import { Stomp } from '@stomp/stompjs';
// @ts-ignore
import * as SockJS from 'sockjs-client';
import {Component, AfterViewInit} from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  title = 'font-end-socket';
  private serverUrl = 'http://localhost:8888/socket';
  // @ts-ignore
  private stompClient;
  public message: any;
  private box: any;
  // @ts-ignore
  constructor() {
    // @ts-ignore
    this.initializeWebSocketConnection();
  }

  // tslint:disable-next-line:typedef
  initializeWebSocketConnection(){
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    // tslint:disable-next-line:only-arrow-functions typedef
    this.stompClient.connect({}, () => {
      // @ts-ignore
      this.stompClient.subscribe('/chat', ( message) => {
        if (message.body) {
          // @ts-ignore
          box.innerHTML += '<div class=\'message\'>' + message.body + '</div>';
          // @ts-ignore
        }
      });
    });
  }
  // tslint:disable-next-line:typedef
  sendMessage(){
    this.stompClient.send('/app/send/message', {}, this.message);
    this.message = '';
  }

  ngAfterViewInit(): void {
    this.box = document.getElementById('box');
  }


}


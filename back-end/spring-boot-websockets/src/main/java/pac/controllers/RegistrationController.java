package pac.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pac.model.User;
import pac.services.MailService;

import javax.mail.MessagingException;

@RestController
public class RegistrationController {


    @Autowired
    public MailService mailService;

    @GetMapping("send-mail")
    public String send(){

        User user = new User();
        user.setEmailAddress("ndkien2598@gmail.com");

        mailService.sendEmail(user);

        return "success";
    }

    @GetMapping("send-file")
    public String sendFile() throws MessagingException {
        User user = new User();
        user.setEmailAddress("ndkien2598@gmail.com");
        mailService.sendMailWithFile();
        return "success";
    }

}

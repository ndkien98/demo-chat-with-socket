package pac.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pac.model.User;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class MailService {

    /*
     * The Spring Framework provides an easy abstraction for sending email by using
     * the JavaMailSender interface, and Spring Boot provides auto-configuration for
     * it as well as a starter module.
     */
    @Autowired
    public JavaMailSender javaMailSender;

    @Value("${path.file}")
    public String pathFile;

    public void sendEmail(User user) {

        /*
         * This JavaMailSender Interface is used to send Mail in Spring Boot. This
         * JavaMailSender extends the MailSender Interface which contains send()
         * function. SimpleMailMessage Object is required because send() function uses
         * object of SimpleMailMessage as a Parameter
         */

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmailAddress());
        mail.setSubject("Testing mail API");
        mail.setText("Hello baby");
        System.out.println(pathFile);
        javaMailSender.send(mail);
    }


    public void sendMailWithFile() throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        helper = new MimeMessageHelper(message,true);
        helper.setTo("ndkien2598@gmail.com");
        helper.setText("alo 1234 ae");

        FileSystemResource fileSystemResource = new FileSystemResource(new File(pathFile + "1.jpg"));
        helper.addAttachment("test file",fileSystemResource);

        javaMailSender.send(message);
    }



}
